import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {innerFrom} from "rxjs/internal/observable/innerFrom";

@Component({
    selector: 'app-post-examples',
    templateUrl: './post.componentExamples.html',
    styleUrls: ['./post.componentExamples.scss']
})
export class PostComponentExamples implements OnInit {
    strongText = '';
    title = '1234567';
    form: FormGroup;
    formTwo: FormGroup;

    backgroundToggle = false;
    toggle = false;

    arr = [1, 1, 2, 3, 5, 8, 13];
    objs = [
        {
            title: 'post-examples 1', author: 'Vinty',
            comments: [
                {name: 'Max', text: 'lorem1'},
                {name: 'Tax', text: 'lorem2'},
                {name: 'Jak', text: 'lorem3'},
                {name: 'Suk', text: 'lorem4'},
            ]
        },
        {
            title: 'post-examples 2', author: 'Jarik',
            comments: [
                {name: 'Lu', text: 'lorem10'},
                {name: 'Xor', text: 'lorem20'},
                {name: 'Mos', text: 'lorem30'},
                {name: 'Gross', text: 'lorem40'},
            ]
        }
    ]

    now: Date = new Date();
    protected readonly innerFrom = innerFrom;

    constructor(private fb: FormBuilder) {
    }

    onInputStrong(event) {
        this.strongText = (<HTMLInputElement>event.target).value;
    };

    onClick() {
    }

    onBlur(s: string) {
        this.strongText = s;
    }

    onInput2(event: any) {
        this.title = event.target.value;
    }

    ngOnInit(): void {
        this.form = this.fb.group({
            text: [],
            text2: [],
            text3: ['123sdg', Validators.required],
            group: this.fb.group({
                text4: ['wsdfg']
            })
        });
        this.form.valueChanges.subscribe(res => console.log(this.form.valid));
    }
}
