import {Component} from '@angular/core';

@Component({
    selector: 'app-directive-components-structure',
    templateUrl: './directive-components-structure.component.html',
    styleUrls: ['./directive-components-structure.component.css']
})
export class DirectiveComponentsStructureComponent {
    isVisible = true;
}
