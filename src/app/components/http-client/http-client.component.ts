import {Component, OnInit} from '@angular/core';
import {ITodo, TodosService} from "../../services/todos.service";

@Component({
    selector: 'app-http-client',
    templateUrl: './http-client.component.html',
    styleUrls: ['./http-client.component.css']
})
export class HttpClientComponent implements OnInit {

    todos: ITodo[] = [];
    loading = false;
    todoTitle = '';
    error = '';

    constructor(private todoService: TodosService) {
    }

    ngOnInit(): void {
        // fetch('https://jsonplaceholder.typicode.com/todos/1')
        //     .then(response => response.json())
        //     .then(json => console.log(json))
        this.fetchTodos();
    }

    addTodo() {
        if (!this.todoTitle.trim()) {
            return;
        }
        const newTodo: ITodo = {
            title: this.todoTitle,
            completed: false
        }
        this.todoService.addTodo(newTodo)
            .subscribe(todo => {
                this.todos.unshift(todo);
                this.todoTitle = '';
            })
    }

    fetchTodos() {
        this.loading = true;
        this.todoService.fetchTodos()
            .subscribe(response => {
                this.todos = response;
                this.loading = false;
            })
    }

    removeTodo(id: number) {
        this.todoService.removeTodo(id)
            .subscribe(response => {
                this.todos = this.todos.filter(t => t.id !== id)
            })
    }

    completeTodo(id: number) {
        this.todoService.completeTodo(id)
            .subscribe(todo => {
                this.todos.find(t => t.id === todo.id).completed = true;
            })
    }
}
