import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-custom-ng-model',
  templateUrl: './custom-ng-model.component.html',
  styleUrls: ['./custom-ng-model.component.css']
})
export class CustomNgModelComponent implements OnInit{
    appState: 'on';

    handleChange() {
        console.log(this.appState);
    }

    ngOnInit(): void {
        this.appState = 'on'
    }
}
