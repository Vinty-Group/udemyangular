import {Component, OnInit} from '@angular/core';
import {FormArray, FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {MyValidators} from "../../MyValidators";


type test = {
    email: [],
    password: [],
    address: {
        country: [],
        city: []
    },
    skills: []
}

@Component({
    selector: 'app-work-with-forms',
    templateUrl: './work-with-forms.component.html',
    styleUrls: ['./work-with-forms.component.css']
})
export class WorkWithFormsComponent implements OnInit {
    form: FormGroup;
    submitForm = false;


    constructor(private fb: FormBuilder) {

    }

    ngOnInit(): void {
        // this.form = new FormGroup({
        //     email: new FormControl<{value: string, errors: {}}>('', [Validators.email, Validators.required, MyValidators.restrictedEmails]),
        //     password: new FormControl('', [Validators.required, Validators.minLength(2)]),
        //     address: new FormGroup({
        //         country: new FormControl('ru') ,
        //         city: new FormControl('Москва', Validators.required)
        //     }),
        //     skills: new FormArray([])
        // });
        this.form = this.fb.group({
            // 1-е - начальное значение. 2-й валидатор или массив валидаторов, 3-й асинхронный валидатор иил их массив
            email: ['', [Validators.email, Validators.required, MyValidators.restrictedEmails], [MyValidators.uniqEmail]],
            password: ['', [Validators.required, Validators.minLength(3)]],
            address: this.fb.group({
                country: ['ru'],
                city: ['Москва', Validators.required]
            }),
            skills: this.fb.array([])
        });
    }

    submit() {
        this.submitForm = true
        if (this.form.valid) {
            console.log('Form submitted!', this.form)
            const formData = {...this.form.value};
            console.log(formData);
        }
        this.form.reset();
    }

    setCapital() {
        const cityMap = {
            ru: 'Москва',
            ua: 'Киев',
            by: 'Минск'
        }
        const cityKey = this.form.get('address').get('country').value;
        const city = cityMap[cityKey];
        this.form.patchValue({
            address: {city}
        })
    }

    addSkill() {
        const control = new FormControl('');
        // (<FormArray>this.form.get('skills')).push()
        (this.form.get('skills') as FormArray).push(control);
    }
}
