import { Component } from '@angular/core';

@Component({
  selector: 'app-pipes-lesson',
  templateUrl: './pipes-lesson.component.html',
  styleUrls: ['./pipes-lesson.component.css']
})
export class PipesLessonComponent {
    e: number = Math.E;
    str: string = 'hello, hello!!! welcome to to our dungeon...';
    date: Date = new Date();
    myFloat: number = 0.42;

    obj = {
        a: 1,
        b: {
            c: 2,
            d: {
                e: 3,
                f: 4
            }
        }
    }

}
