import {Component, ElementRef, EventEmitter, OnInit, Output, ViewChild} from '@angular/core';
import {IPost} from "../../model/IPost.model";

@Component({
    selector: 'app-post-form',
    templateUrl: './post-form.component.html',
    styleUrls: ['./post-form.component.css']
})
export class PostFormComponent implements OnInit {

    @Output() onAdd: EventEmitter<IPost> = new EventEmitter<IPost>();
    // Обращение к дому
    @ViewChild('titleInput') inputRef: ElementRef;
    title: string = '';
    text: string = '';

    constructor() {
    }

    ngOnInit(): void {
    }

    addPost() {
        if (this.title.trim() && this.text.trim()) {
            const post: IPost = {
                title: this.title,
                text: this.text
            }
            this.onAdd.emit(post);
            this.title = this.text = '';
        }
    }

    focusTitle() {
        // nativeElement - это ДОМ элемент со всеми его методами
        this.inputRef.nativeElement.focus();
    }
}
