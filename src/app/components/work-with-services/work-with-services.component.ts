import { Component } from '@angular/core';
import {AppCounterService} from "../../services/app-counter.service";
import {LocalCounterService} from "../../services/local--counter.service";

@Component({
  selector: 'app-work-with-services',
  templateUrl: './work-with-services.component.html',
  styleUrls: ['./work-with-services.component.css'],
  providers: [LocalCounterService]
})
export class WorkWithServicesComponent {
constructor(public appCounterService: AppCounterService,
            public localCounterService: LocalCounterService) {

}
}
