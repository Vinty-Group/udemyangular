import {
    AfterContentChecked,
    AfterContentInit,
    AfterViewChecked,
    AfterViewInit,
    ChangeDetectionStrategy,
    Component,
    ContentChild,
    DoCheck,
    ElementRef,
    EventEmitter,
    Input,
    OnChanges,
    OnDestroy,
    OnInit,
    Output,
    SimpleChanges
} from '@angular/core';
import {IPost} from "../../model/IPost.model";

@Component({
    selector: 'app-post',
    templateUrl: './post.component.html',
    styleUrls: ['./post.component.css'],
    // Эта стратегия - перерисовывать, если поменялся Input()
    // changeDetection: ChangeDetectionStrategy.OnPush
})
export class PostComponent implements OnInit, OnChanges, DoCheck, AfterContentInit, AfterContentChecked, AfterViewInit, AfterViewChecked, OnDestroy {

    @Input() myPost: IPost;
    @Output() onRemove = new EventEmitter<IPost>();
    // static: true - значит, что все в методе ngOnInit()
    // info находится   <div #info> - app.component.html
    @ContentChild('info', {static: true}) infoRef: ElementRef;

    constructor() {
        // console.log('constructor');
    }

    removePost() {
        this.onRemove.emit(this.myPost)
    }

    ngOnInit(): void {
        // console.log(this.infoRef.nativeElement)
        // console.log('ngOnInit');
    }

    // выполняется до ngOnInit()
    ngOnChanges(changes: SimpleChanges): void {
        // console.log('ngOnChanges', changes);
    }

    ngDoCheck(): void {
        // console.log('DoCheck');
    }

    ngAfterContentInit() {
        // console.log('ngAfterContentInit')
    }

    ngAfterContentChecked(): void {
        // console.log('ngAfterContentChecked')
    }

    ngAfterViewInit() {
        // console.log('ngAfterViewInit')
    }

    ngAfterViewChecked() {
        // console.log('ngAfterViewChecked')
    }

    ngOnDestroy() {
        // console.log('ngOnDestroy')
    }
}
