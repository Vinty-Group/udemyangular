import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";

export interface IPost {
  title: string
  text: string
}
@Component({
  selector: 'app-pipe-list',
  templateUrl: './pipe-list.component.html',
  styleUrls: ['./pipe-list.component.css']
})
export class PipeListComponent implements OnInit {

  p: Promise<string> = new Promise<string>(resolve => {
    setTimeout(() => {
      resolve('Promise Resolved')
    }, 4000)
  });

  myDate$: Observable<Date> = new Observable(obs => {
    setInterval(() => {
      obs.next(new Date())
    }, 1000)
  })

  search = '';
  searchField = 'title';
  posts: IPost[] = [
    {title: 'Bear', text: 'Лучшее пиво в мире'},
    {title: 'Bread', text: 'The best bread in the world'},
    {title: 'JavaScript', text: 'Уж лучше бы TypeScript'},
  ]

  addPost() {
    this.posts.unshift({
      title: 'Angular 15',
      text: 'Просто пример'
    })
  }

  // date: Date;
  ngOnInit(): void {
    // this.myDate$.subscribe(date => {
    //   this.date = date;
    // })
  }
}
