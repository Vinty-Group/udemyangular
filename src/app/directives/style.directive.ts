import {Directive, ElementRef, HostBinding, HostListener, Input, Renderer2} from '@angular/core';

@Directive({
    selector: '[appStyle]'
})
export class StyleDirective {

    constructor(private elRef: ElementRef, private render: Renderer2) {
    }
    // 2 способа. Или так или так:  [appStyle]="'red'" fontWeight="bold"
    @Input('appStyle') color: string;
    @Input() dStyles: {border?: string, fontWeight?: string, borderRadius?: string}
    @Input() fStyles: string;
    // или через @HostBinding
    @HostBinding('style.font-size') fontSize = null;

    @HostListener('click', ['$event.target']) onClick(event: Event){
        console.log(event);
    }

    @HostListener('mouseenter') onMouseEnter(){
        this.render.setStyle(this.elRef.nativeElement, 'color', this.color);
        this.render.setStyle(this.elRef.nativeElement, 'fontWeight', this.dStyles?.fontWeight);
        this.render.setStyle(this.elRef.nativeElement, 'border', this.dStyles?.border);
        this.render.setStyle(this.elRef.nativeElement, 'borderRadius', this.dStyles?.borderRadius);
        this.fontSize = this.fStyles;
    }

    @HostListener('mouseleave') onMouseLeave(){
        this.render.setStyle(this.elRef.nativeElement, 'color', null);
        this.render.setStyle(this.elRef.nativeElement, 'fontWeight', null);
        this.render.setStyle(this.elRef.nativeElement, 'border', null);
        this.render.setStyle(this.elRef.nativeElement, 'borderRadius', null);
        this.fontSize = null;
    }
}
