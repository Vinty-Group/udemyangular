import {Directive, Input, TemplateRef, ViewContainerRef} from '@angular/core';

@Directive({
    selector: '[appIfNot]'
})
export class IfnotDirective {
    @Input('appIfNot') set ifNot(condition: boolean){
        if (!condition){
            // показать элементы
            this.viewContainer.createEmbeddedView(this.templateRef);
        } else {
            // скрыть
            this.viewContainer.clear();
        }
    }

    // templateRef - то что внутри <ng-template>
    // viewContainer - указывает на сам  ng-template
    constructor(private templateRef: TemplateRef<any>,
                private viewContainer: ViewContainerRef) {
    }
}
