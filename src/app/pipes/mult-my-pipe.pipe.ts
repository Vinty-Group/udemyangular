import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'multiMyPipe'
})
export class MultiMyPipePipe implements PipeTransform {

  // num - входной параметр до вертикальной черты
  // pow - параметр ПОСЛЕ двоеточия
  transform(num: number, pow: number = 2): number {
    return num * pow;
  }
}
