import {NgModule, Provider} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

import {AppComponent} from './app.component';
import {PostComponentExamples} from './components/post-examples/post.componentExamples';
import {PostFormComponent} from './components/post-form/post-form.component';
import {PostComponent} from './components/post/post.component';
import {StyleDirective} from './directives/style.directive';
import {DirectiveComponentsComponent} from './components/directive-components/directive-components.component';
import {
    DirectiveComponentsStructureComponent
} from './components/directive-components-structure/directive-components-structure.component';
import {IfnotDirective} from './directives/ifnot.directive';
import {PipesLessonComponent} from './components/pipes-lesson/pipes-lesson.component';
import {MultiMyPipePipe} from './pipes/mult-my-pipe.pipe';
import {ExMarksPipe} from './pipes/ex-marks.pipe';
import {PipeListComponent} from './components/pipe-list/pipe-list.component';
import {FilterPipe} from './pipes/filter.pipe';
import {WorkWithServicesComponent} from './components/work-with-services/work-with-services.component';
import {WorkWithFormsComponent} from './components/work-with-forms/work-with-forms.component';
import {CustomNgModelComponent} from './components/custom-ng-model/custom-ng-model.component';
import {SwitchComponent} from './components/switch/switch.component';
import {HttpClientComponent} from './components/http-client/http-client.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {AuthInterceptorService} from "./services/auth.interceptor.service";


const INTERCEPTOR_PROVIDER: Provider = {
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptorService,
    multi: true
}

@NgModule({
    declarations: [
        AppComponent,
        PostComponentExamples,
        PostFormComponent,
        PostComponent,
        StyleDirective,
        DirectiveComponentsComponent,
        DirectiveComponentsStructureComponent,
        IfnotDirective,
        PipesLessonComponent,
        MultiMyPipePipe,
        ExMarksPipe,
        PipeListComponent,
        FilterPipe,
        WorkWithServicesComponent,
        WorkWithFormsComponent,
        CustomNgModelComponent,
        SwitchComponent,
        HttpClientComponent,
    ],
    imports: [
        BrowserModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule
    ],
    providers: [
        INTERCEPTOR_PROVIDER
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
