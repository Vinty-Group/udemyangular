import {Component} from '@angular/core';
import {IPost} from "./model/IPost.model";



@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
// title = 'Udemy';
    posts: IPost[] = [
        {title: 'Хочу выучить Angular', text: 'Все еще учу компоненты', id: 1},
        {title: 'Следующий блок', text: 'Будет про директивы и про пайпы', id: 2}
    ]

    updatePosts(post: IPost) {
        // добавляем в начало массива элемент
        // this.posts.unshift(post);
        // в начало. Создаем новый что бы отрабатывали Pipes
        this.posts = [post, ...this.posts];
        // в конец
        // this.posts = [...this.posts, post];
    }

    removePost(post: IPost) {
        this.posts = this.posts.filter(p => p !== post)
    }
}
