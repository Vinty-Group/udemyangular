import {Injectable} from '@angular/core';
import {HttpEvent, HttpEventType, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {Observable, tap} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor{

  constructor() { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    console.log('Intercept req: ', req)
    const  cloned = req.clone({
      headers: req.headers.append('AUTH_Token', 'KJGHKJ256LKHLJKHkl378dasfadfhssdfh43KLMBHNjMNB')
    })
    return next.handle(cloned).pipe(
        tap(event => {
          if (event.type === HttpEventType.Response){
            console.log('Interceptor response', event)
          }
        })
    );
  }
}
