import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from "@angular/common/http";
import {catchError, delay, EMPTY, map, Observable} from "rxjs";

export interface ITodo {
    completed: boolean
    title: string
    id?: number
}

@Injectable({
    providedIn: 'root'
})
export class TodosService {

    constructor(private http: HttpClient) {
    }

    addTodo(todo: ITodo): Observable<ITodo> {
        return this.http.post<ITodo>('https://jsonplaceholder.typicode.com/todos', todo, {
            headers: new HttpHeaders({
                'MyNewHeaders': 'HHHHHHH'
            })
        });
    }

    fetchTodos1(): Observable<ITodo[]> {
        let params = new HttpParams()
        params = params.append('_limit', 4)
        params = params.append('custom', 'bla-bla-bla')
        return this.http.get<ITodo[]>('https://jsonplaceholder.typicode.com/todos', {
            params
        })
            .pipe(
                delay(500),
                catchError(error => {
                    console.log('ERROR: ', error.message);
                    return EMPTY
                })
            )
    }

    fetchTodos(): Observable<ITodo[]> {
        let params = new HttpParams()
        params = params.append('_limit', 4)
        params = params.append('custom', 'bla-bla-bla')
        return this.http.get<ITodo[]>('https://jsonplaceholder.typicode.com/todos', {
            params,
            // для более детального вида
            observe: 'response'
        })
            .pipe(
                map(response => {
                    console.log('Response', response)
                    return response.body
                }),
                catchError(error => {
                    console.log('ERROR: ', error.message);
                    return EMPTY
                })
            )
    }

    removeTodo(id: number): Observable<void> {
        return this.http.delete<void>(`https://jsonplaceholder.typicode.com/todos/${id}`)
    }

    completeTodo(id: number): Observable<ITodo> {
        return this.http.put<ITodo>(`https://jsonplaceholder.typicode.com/todos/${id}`, {
            completed: true
        }, {
            // responseType: "json" - по дефолту
            // responseType: "blob"
            // responseType: "text"
        })
    }
}
